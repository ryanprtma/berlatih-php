<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class likes extends Model
{
    protected $table='likes';
    protected $fillable=['berita_id', 'user_id', 'jumlah_like'];

    public function user(){
        return $this -> belongsTo ('App\user');
    }

    public function berita (){
        return $this -> belongsTo('App\berita');
    }
}
