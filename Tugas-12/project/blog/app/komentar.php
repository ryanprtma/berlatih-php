<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komentar extends Model
{
    protected $table='komentar';
    protected $fillable=['berita_id','user_id','isi'];

    public function user(){
        return $this->belongsTo('App\user');
    }

    public function berita(){
        return $this->belongsTo('App\berita');
    }
}
