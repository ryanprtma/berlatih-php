<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LikesController extends Controller
{
    public function update(Request $request){
        $berita=$request->berita_id;
        $likes = new likes;
        
        $likes->jumlah_like=$request->jumlah_like;
        $likes->berita_id=$request->berita_id;
        $likes->user_id=Auth::id();

        $likes->save();
        
        // return redirect()->back();
        return redirect('/berita.$berita');


    }
}   
