<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\berita;//panggil model bertia
use File;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()//read data
    {
        $berita = berita::all();
        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view('berita.create',compact('kategori'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'judul' =>'required',
            'content'=>'required',
            'kategori_id'=>'required',
            'thumbnail' =>'required|image|mimes:jpeg,png,jpg,gif,svg max:2048'
        ]);

        $thumbnailName = time().'.'.$request->thumbnail->extension();//dibuat unik nama gambarnya
        $request->thumbnail->move(public_path('gambarBerita'), $thumbnailName);
        $berita = new berita;


        $berita->judul = $request->judul;
        $berita->content = $request->content;
        $berita->kategori_id = $request->kategori_id;
        $berita->thumbnail = $thumbnailName;
        $berita->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        @$berita= berita::findOrFail($id);
        return view('berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $berita = berita::findOrFail($id);
        return view('berita.edit', compact('berita', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' =>'required',
            'content'=>'required',
            'kategori_id'=>'required',
            'thumbnail' =>'image|mimes:jpeg,png,jpg,gif,svg max:2048'
        ]);
        $berita = berita::find($id);
        if($request->has('thumbnail')){
            $thumbnailName = time().'.'.$request->thumbnail->extension();//dibuat unik nama gambarnya
            $request->thumbnail->move(public_path('gambarBerita'), $thumbnailName);


            $berita->judul = $request->judul;
            $berita->content = $request->content;
            $berita->kategori_id = $request->kategori_id;
            $berita->thumbnail = $thumbnailName;

        }else{
            $berita->judul = $request->judul;
            $berita->content = $request->content;
            $berita->kategori_id = $request->kategori_id;
        }

        $berita->update();
        return redirect('/berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $berita = berita::find($id);
        $path = "gambarBerita/";
        File::delete($path .$berita->thumbnail);
        $berita->delete();
        return redirect('/berita');
    }
}
