<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use File;

class ProfileController extends Controller
{
    Public function index(){
        $profile = Profile::where('user_id', Auth::id())->first();
        return view('profile.index', compact('profile'));
    }

    Public function Update(Request $request, $id ){
        $profile = Profile::find($id);
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->bio = $request->bio;

        $profile->save();

        return redirect('/profile');
    }
}