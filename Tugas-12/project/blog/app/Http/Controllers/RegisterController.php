<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function form()
    {
    	return view('halaman.form');
    }

    public function user_post(Request $request)
    {
    	$nama = $request["nama"];
    	$belakang = $request["belakang"];
    	return view('halaman.user', ["belakang" => $belakang, "nama" => $nama]);
    }

    public function user()
    {
    	return view('halaman.user');
    }
}
?>
