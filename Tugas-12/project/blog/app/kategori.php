<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    //data data yg inign dimanipulasi
    protected $table='kategori';
    protected $fillable=['nama','deskripsi'];
    //relasi has many ke berita
    public function berita(){
        return $this->hasMany('App\berita');
    }
}