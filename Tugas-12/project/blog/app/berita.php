<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class berita extends Model
{
    //data data yg inign dimanipulasi
    protected $table='berita';
    protected $fillable=['judul','content','kategori_id','thumbnail'];

    public function kategori(){
        return $this->belongsTo('App\kategori');
    }

    public function komentar(){
        return $this->hasMany('App\komentar');
    }

    public function likes(){
        return $this->hasMany('App\likes');
    }
}
