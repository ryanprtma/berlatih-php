<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/welcomeuser', 'RegisterController@user');
Route::post('/welcomeuser', 'RegisterController@user_post');

Route::get('/daftar', 'RegisterController@form');

Route::get('/', 'BeritaController@index',function(){
	// return view('berita.index');
});

// Route::get('/', 'IndexController@index');
// Route::get('/ppp', 'HomeController@index');
Route::get('/dashboardone', 'HomeController@index',function(){
});


Route::get('/data-table', function(){
	return view('table.datatable');
});

Route::get('/table', function(){
	return view('table.table');
});

Route::get('/cast/create', 'CastController@create');
Route::post('cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{id}', 'CastController@show');
Route::get('/cast/{id}/edit', 'CastController@edit');
Route::put('/cast/{id}', 'CastController@update');
Route::delete('/cast/{id}', 'CastController@destroy');
Route::resource('berita', 'BeritaController');



Route::middleware(['auth'])->group(function () {
    //CRUD Kategori
	Route::get('/kategori/create', 'KategoriController@create');//pendaftaran route/alamat
	Route::post('kategori', 'KategoriController@store');
	Route::get('/kategori', 'KategoriController@index');
	Route::get('/kategori/{id}', 'KategoriController@show');
	Route::get('/kategori/{id}/edit', 'KategoriController@edit');
	Route::put('/kategori/{id}', 'KategoriController@update');
	Route::delete('/kategori/{id}', 'KategoriController@destroy');

	//Route Profile
	Route::resource('profile','ProfileController')->only(['index','update']);

	//Route komentar
	Route::resource('komentar','KomentarController')->only(['store']);

	Route::resource('likes', 'LikesController')->only(['update']);
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
