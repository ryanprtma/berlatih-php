@extends('layout.master')
@section('judul')
    Edit Kategori
@endsection

@section('content')
<div>
    
    <form action="/kategori/{{$kategori->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" value="{{$kategori->nama}}" name="nama" id="Masukkan Nama" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="deskripsi">Deskripsi</label>
            <textarea name="deskripsi" class="form-control"> {{$kategori->deskripsi}}</textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection