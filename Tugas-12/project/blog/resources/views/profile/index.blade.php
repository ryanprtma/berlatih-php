@extends('layout.master')
@section('judul')
    Update Profile
@endsection

@section('content')
<div>
        <form action="/profile/{{$profile->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label >Nama</label>
                <input type="text" value="{{$profile->user->name}}" class="form-control" disabled>
            </div>
            <div class="form-group">
                <label >Email</label>
                <input type="email" value="{{$profile->user->email}}" class="form-control" disabled>
            </div>
            <div class="form-group">
                <label for="title">Umur</label>
                <input type="number" value="{{$profile->umur}}" class="form-control" name="umur" id="Masukkan Umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Alamat</label>
                <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Bio</label>
                <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
                @error('biodata')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Ubah</button>
        </form>
</div>
@endsection


