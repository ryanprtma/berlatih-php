<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <section>
        <form action="/welcomeuser" method="POST">@csrf
            <label for="fname">First name:</label><br>
            <input type="text" id="fname" name="nama"><br><br>
            <label for="lname">Last name:</label><br>
            <input type="text" id="lname" name="belakang"><br><br>
            <p>Gender:</p>
            <input type="radio" id="male" value="male">
                <label for="male" name="Gender">
                    Male
                </label><br>
            <input type="radio" id="female" value="female" name="Gender">
                <label for="female">
                    Female
                </label><br>
            <input type="radio" id="other" value="other">
                <label for="other" name="Gender">
                    Other
                </label><br><br>
            <label for="kebangsaan">Nationality:</label><br><br>
                <select id="indonesia">
                    <option value="indonesia">Indonesia</option>
                    <option value="other">Other</option>
                </select><br><br>
            <label for="language">Language Spoken:</label><br><br>
            <input type="checkbox" id="indonesia" name="language" value="bahasa_indonesia">
                <label for="indonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" id="inggris" name="language" value="english">
                <label for="inggris">English</label><br>
            <input type="checkbox" id="other" name="other" value="other">
                <label for="other">other</label><br><br>
            <label for="bio">Bio:</label><br><br>
                <textarea id="bio" rows="4" cols="50">
                </textarea><br><br>
            <input type="submit" name="masuk" value="Sign Up">
        </form>
    </section>
</body>
</html>