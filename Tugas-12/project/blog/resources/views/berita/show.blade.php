@extends('layout.master')
@section('judul')
    Detail Berita
@endsection
<?php
if(isset($_POST['update']))
{    
    $result = mysqli_query("UPDATE likes SET jumlah_like=jumlah_like + 1 WHERE id=1");
    
}
?>
@section('content')
    <img src="{{asset('gambarBerita/'.$berita->thumbnail)}}" alt="..." class="card-img-top">
    <h1>{{$berita->judul}}</h1>
    <p>{{$berita->content}}</p>
    <h3>Komentar</h3>
    @foreach ($berita->komentar as $item)
    <div class="card">
        <div class="card-body">
            <h6 class="card-subtitle mb-2 text-muted">{{$item->user->name}}</h6>
            <p class="card-text">{{$item->isi}}</p>
        </div>
    </div>
    @endforeach
    <form action="/komentar" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Komentar</label>
            <input type="hidden" value={{$berita->id}} name="berita_id">
            <textarea name="isi" class="form-control" placeholder="Tanggapi"></textarea>
            @error('isi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary my-2">Kirim</button>
    </form>
    <a href="/berita" class="btn btn-secondary">Kembali</a>
@endsection