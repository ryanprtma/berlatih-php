@extends('layout.master')
@section('judul')
    Berita
@endsection
@section('content')

@auth
<a href="/berita/create" class="btn btn-primary my-2" >Tambah</a>
@endauth
    <div class="row">
        @forelse ($berita as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset('gambarBerita/'.$item->thumbnail)}}" alt="..." class="card-img-top">
                    <div class="card-body">
                        <span class="badge badge-info">{{$item->kategori->nama}}</span>
                        <h3 class="card-title">{{$item->judul}}</h3>
                        <p class="card-text">{{Str::limit($item->content,30)}}</p>
                        @auth
                        <form action="/berita/{{$item->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Go</a>
                        {{-- id pada show@beritacontroller --}}
                            <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" name="" id="" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                        @endauth
                        @guest
                        <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Go</a>
                        @endguest
                        
                </div>
            </div>
        @empty
            
            <h4>Data Berita Belum Ada</h4>
        @endforelse
    </div>
@endsection