@extends('layout.master')
@section('judul')
    Edit Cast
@endsection

@section('content')
<div>
    
    <form action="/berita/{{$berita->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label >Judul Berita</label>
            <input type="text" value="{{$berita->judul}}" class="form-control" name="judul" >
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label >Content</label>
            <textarea name="content" class="form-control">{{$berita->content}}</textarea>
            @error('content')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Kategori</label>
            <select  name="kategori_id" class="form-control" id="">
                <option value="">---Pilih Kategori---</option>
                @foreach ($kategori  as $item) 
                    @if($item->id===$berita->kategori_id)
                        <option value="{{$item->id}}"selected>{{$item->nama}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endif
                @endforeach
            </select>
            @error('kategori_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label >Thumbnail</label>
            <input type="file" class="form-control" name="thumbnail" class="form-control">
            @error('thumbnail')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection