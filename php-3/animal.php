<?php
class Animal{
    public $name;
    public $legs=4;
    public $cold_blooded="no";

    public function __construct($name){
        $this->name=$name;
    }

    public function get_name(){
        return "<br>Name : " . $this->name . "<br>";
    }
    function get_legs (){
        return "Legs : " . $this->legs . "<br>";
    }

    public function get_cold_blooded(){
		echo "Cold Blooded : " . $this->cold_blooded . "<br>";
	}
}
?>

