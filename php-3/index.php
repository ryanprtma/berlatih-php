<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("shaun");

echo $sheep->get_name();
echo $sheep->get_legs();
echo $sheep->get_cold_blooded();

$sungokong = new Ape("kera sakti");
echo $sungokong->get_name();
echo $sungokong->get_legs();
echo $sungokong->get_cold_blooded();
$sungokong->yell(); 

$kodok = new frog("buduk");
echo $kodok->get_name();
echo $kodok->get_legs();
echo $kodok->get_cold_blooded();
$kodok->jump(); 
?>